const uuid = require('uuid/v1');

module.exports = (req, res, next) => {
  res.header('X-Fake-Token', uuid());
  next();
};
