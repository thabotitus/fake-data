# FakeData

This is a small project to generate data on the fly and serve as a normal API would.
Use case: To build user interfaces that are driven by data.

## Getting started
Install `json-server`
```
npm install -g json-server
```

Install depencencies
```
npm install
```

Run it - it will run on port 3001. You can change it.
```
npm start
```

#### Routes / Endpoints
Routes get automatically generated for you:

```
GET    /licencePools
GET    /licencePools/:id
POST   /licencePools
PUT    /licencePools/:id
PATCH  /licencePools/:id
DELETE /licencePools/:id
```

#### Filter

```
GET /licensePools?virtualAccount=DNA
GET /licensePools?virtualAccount=BCIT&compliance=Non-Compliant
```

#### Paginate
Use `_page` and optionally `_limit` to paginate returned data.
In the `Link` header you'll get `first`, `prev`, `next` and `last` links.

```
GET /licensePools?_page=2&_limit=5
```

See more options at [JSON SERVER DOCS](https://github.com/typicode/json-server#getting-started)