const utils = require('../utils');

function generateOverview () {
  const totalNumberOfAssets =     utils.randomNumber(3000, 5000);
  const numberOfMonitoredAssets = utils.randomNumber(500, 3000);
  const numberOfManagedHardware = utils.randomNumber(500, 5000);
  const numberOfManagedSoftware = utils.randomNumber(500, 5000);

  return { 
    totalNumberOfAssets,
    numberOfMonitoredAssets,
    numberOfManagedHardware,
    numberOfManagedSoftware
   }
}

module.exports = {
  overview: generateOverview()
};