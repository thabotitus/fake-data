const digitalWallet = require('./digital-wallet.js');
const assets = require('./assets.js');
const assetsOverview = require('./assetsOverview.js');

module.exports = () => ({
  licencePools: digitalWallet.licensePools,
  assets: assets.assets,
  assetsOverview: assetsOverview.overview
});