const faker = require('faker');
const uuid = require('uuid/v1');
const utils = require('../utils');

function generateAssetList () {
  let assets = [];
  const count = 20;

  for (let i = 0; i <= count;  i++) {
    const sysId = uuid();
    const ciName = faker.company.companyName();
    const internalName = faker.company.catchPhraseNoun();
    const make = 'Lenovo';
    const model = 'XJ-9';
    const ciClass = faker.system.commonFileType();
    const serialNumber = faker.random.hexaDecimal;
    const hostName = faker.internet.hostName;
    const ipAddress = faker.internet.ip();
    const department = faker.commerce.department();
    const location = `${faker.address.streetAddress()}, ${faker.address.city()}, ${faker.address.zipCode()}`;
    
    assets.push({
      sysId,
      ciName,
      internalName,
      make,
      model,
      ciClass,
      serialNumber,
      hostName,
      ipAddress,
      department,
      location
    });
  }

  return { assets }
}

module.exports = generateAssetList();