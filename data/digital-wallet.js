const faker = require('faker');
const uuid = require('uuid/v1');
const utils = require('../utils');

function generateLicensePool () {
  let licensePools = [];
  const count = 10;

  for (let i = 0; i <= count;  i++) {
    const sysId = uuid();
    const licensePoolName = faker.company.companyName();
    const purchased = utils.randomNumber(800, 200);
    const inUse = utils.randomNumber(400, 50);
    const balance = (purchased - inUse);
    const compliance = Math.random() > 0.5 ? 'Compliant': 'Non-Compliant';
    const tickets = utils.randomNumber(5, 60);
    const devicesInUse = utils.randomNumber(10, 30);
    const virtualAccount = ['DNA', 'BCIT', 'Default', 'TRG'][utils.randomNumber(0,3)];
    const licenses = generateLicenses(licensePoolName);
    
    licensePools.push({
      sysId,
      licensePoolName,
      purchased,
      inUse,
      balance,
      compliance,
      tickets,
      devicesInUse,
      virtualAccount,
      licenses
    });
  }

  return { licensePools }
}

const generateLicenses = (licensePoolName) => {
  let licenses = [];

  for (let i = 0; i <= utils.randomNumber(1,4);  i++) {
    const sysId = uuid();
    const licenseName = `${licensePoolName}: ${faker.address.streetName()}`;
    const purchased = utils.randomNumber(800, 200);
    const inUse = utils.randomNumber(400, 50);
    const balance = purchased - inUse;
    const type = ['Demo', 'Perceptual', 'Term'][utils.randomNumber(0,2)];
    const status = Math.random() > 0.5 ? 'Expired' : 'Active';
    const startDate = faker.date.past();
    const expiryDate = faker.date.future();
    
    licenses.push({
      sysId,
      licenseName,
      purchased,
      inUse,
      balance,
      type,
      status,
      startDate,
      expiryDate
    });
  }

  return licenses;
}

const randomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

module.exports = generateLicensePool();